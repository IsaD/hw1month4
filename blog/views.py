from django.http import HttpResponse
from django.shortcuts import render
from . import models

def hello_world_text(requests):
    return HttpResponse("Здраствуйте,дорогой клиент, вас привествует книжный магазин 'не придумал'")


def blog_view(requests):
    blog = models.BlogProgrammLang.objects.all()
    return render(requests, 'templates/blog.html', {'blog': blog})